N=10;
//+
Point(9) = {0, 0, 0, N};
//+
Point(10) = {0.1, 0, 0, N};
//+
Point(11) = {0, 0, 0.1, N};
//+
Point(12) = {-0.1, 0, 0, N};
//+
Point(13) = {0, 0, -0.1, N};
//+
Point(14) = {0, 0.2, 0, N};
//+
Point(15) = {0, -0.2, 0, N};
//+
Ellipse(13) = {10, 9, 11, 11};
//+
Ellipse(14) = {12, 9, 11, 11};
//+
Ellipse(15) = {12, 9, 13, 13};
//+
Ellipse(16) = {13, 9, 10, 10};
//+
Ellipse(17) = {14, 9, 10, 10};
//+
Ellipse(18) = {14, 9, 11, 11};
//+
Ellipse(19) = {14, 9, 12, 12};
//+
Ellipse(20) = {14, 9, 13, 13};
//+
Ellipse(21) = {15, 9, 10, 10};
//+
Ellipse(22) = {15, 9, 11, 11};
//+
Ellipse(23) = {15, 9, 12, 12};
//+
Ellipse(24) = {15, 9, 13, 13};
//+
Curve Loop(1) = {18, -22, 21, -17};
//+
Curve Loop(2) = {18, -13, -17};
//+
Plane Surface(1) = {2};
//+
Curve Loop(3) = {22, -13, -21};
//+
Plane Surface(2) = {3};
//+
Curve Loop(4) = {16, -21, 24};
//+
Plane Surface(3) = {4};
//+
Curve Loop(5) = {17, -16, -20};
//+
Plane Surface(4) = {5};
//+
Curve Loop(6) = {15, -20, 19};
//+
Plane Surface(5) = {6};
//+
Curve Loop(7) = {23, 15, -24};
//+
Plane Surface(6) = {7};
//+
Curve Loop(8) = {22, -14, -23};
//+
Plane Surface(7) = {8};
//+
Curve Loop(9) = {18, -14, -19};
//+
Plane Surface(8) = {9};
//+
Surface Loop(1) = {5, 6, 7, 2, 1, 8, 4, 3};
//+
Volume(1) = {1};
