#ifndef LIFEX_FLUID_DYNAMICS_IBM_HPP_
#define LIFEX_FLUID_DYNAMICS_IBM_HPP_

#include "core/source/core_model.hpp"
#include "core/source/geometry/mesh_handler.hpp"
#include "source/numerics/tools.hpp"
#include <deal.II/non_matching/coupling.h>
#include "source/numerics/linear_solver_handler.hpp"
#include "source/numerics/preconditioner_handler.hpp"
#include "source/io/data_writer.hpp"
#include <deal.II/base/parsed_function.h>
#include <deal.II/fe/mapping_q_eulerian.h>

#include <filesystem>

namespace lifex::examples
{
  class IBM : public CoreModel
  {
  public:
    /// Constructor.
    IBM()
      : CoreModel("IBM")
      , triangulation_f(
          std::make_shared<lifex::utils::MeshHandler>(prm_subsection_path,
                                                      mpi_comm))
        , triangulation_s(
            std::make_shared<lifex::utils::MeshHandler>(prm_subsection_path,
                                                        mpi_comm))
        , velocities(0)
        , pressure(0)
        , disp(0)
        , lagrange_mult(0)
        , initial_condition_velocity(Functions::ParsedFunction<dim>(dim))
        , initial_condition_mapping(Functions::ParsedFunction<dim>(dim))
        , preconditioner(prm_subsection_path + " / Preconditioner", matrix, true)
        , linear_solver(prm_subsection_path + " / Linear solver",
                        {"CG", "GMRES", "BiCGStab"},
                        "GMRES")
         {}

    /// Override of @ref CoreModel::declare_parameters.
    virtual void
    declare_parameters(ParamHandler &params) const override;

    /// Override of @ref CoreModel::parse_parameters.
    virtual void
    parse_parameters(ParamHandler &params) override;

    /**
     * @brief Run model simulation.
     *
     * Boundary conditions must be set before this method is called. In
     * particular, if the method is called without previously calling at least
     * one of @ref set_BCs_dirichlet, @ref set_BCs_neumann or @ref set_BCs_periodic,
     * an exception is thrown.
     */
    virtual void
    run() override;


  private:
    /// Mesh fluid.
    std::shared_ptr<utils::MeshHandler> triangulation_f;
    /// Mesh structure.
    std::shared_ptr<utils::MeshHandler> triangulation_s;
    /// Mesh file for fluid.
    std::string prm_file_f;
    /// Mesh file for structure.
    std::string prm_file_s;
    /// Scaling factor.
    double prm_scaling_factor;
    /// Finite element space for velocity.
    std::unique_ptr<FESystem<dim>> fe_v;
    /// Finite element space for pressure.
    std::unique_ptr<FESystem<dim>> fe_p;
    /// Finite element space for mapping.
    std::unique_ptr<FESystem<dim>> fe_m;
    /// Finite element space for Lagrange multipliers.
    std::unique_ptr<FESystem<dim>> fe_lm;
    /// degree of the velocity FE space.
    unsigned int prm_velocity_degree;
    /// degree of the pressure FE space.
    unsigned int prm_pressure_degree;
    /// degree of the displacement from reference domain of structure to actual FE space.
    unsigned int prm_mapping_degree = 1;
    /// degree of the Lagrange multpliers FE space.
    unsigned int prm_lm_degree = 1;
    /// Quadrature formula.
    std::unique_ptr<Quadrature<dim>> quadrature_formula_f;
    /// Quadrature formula.
    std::unique_ptr<Quadrature<dim>> quadrature_formula_s;
    /// Evaluation of @ref fe on a cell for velocity.
    std::unique_ptr<FEValues<dim>> fe_values_v;
    /// Evaluation of @ref fe on a cell for pressure.
    std::unique_ptr<FEValues<dim>> fe_values_p;
    /// Evaluation of @ref fe on a cell for displacement.
    std::unique_ptr<FEValues<dim>> fe_values_m;
    /// Evaluation of @ref fe on a cell for Lagrange multipliers.
    std::unique_ptr<FEValues<dim>> fe_values_lm;
    /// Dof handler for velocity.
    DoFHandler<dim> dof_handler_v;
    /// Dof handler for pressure.
    DoFHandler<dim> dof_handler_p;
    /// Dof handler for displacement.
    DoFHandler<dim> dof_handler_m;
    /// Dof handler for Lagrange multipliers.
    DoFHandler<dim> dof_handler_lm;
    /// Owned dofs.
    std::vector<IndexSet> owned_dofs;
    /// Owned dofs for velocity.
    IndexSet owned_dofs_v;
    /// Owned dofs for pressure.
    IndexSet owned_dofs_p;
    /// Owned dofs for displacement.
    IndexSet owned_dofs_m;
    /// Owned dofs for Lagrange multpliers.
    IndexSet owned_dofs_lm;
    /// Relevant dofs.
    std::vector<IndexSet> relevant_dofs;
    /// Relevant dofs for velocity.
    IndexSet relevant_dofs_v;
    /// Relevant dofs for pressure.
    IndexSet relevant_dofs_p;
    /// Relevant dofs for displacement.
    IndexSet relevant_dofs_m;
    /// Relevant dofs for Lagrange multpliers.
    IndexSet relevant_dofs_lm;
    /// FE values extractor for velocity.
    const FEValuesExtractors::Vector velocities;
    /// FE values extractor for pressure.
    const FEValuesExtractors::Scalar pressure;
    /// FE values extractor for displacement.
    const FEValuesExtractors::Vector disp;
    /// FE values extractor for Lagrange multpliers.
    const FEValuesExtractors::Vector lagrange_mult;
    /// Number of Dof for velocity.
    unsigned int n_u;
    /// Number of Dof for pressure.
    unsigned int n_p;
    /// Number of Dof for displacement.
    unsigned int n_m;
    /// Number of Dof for Lagrange multpliers.
    unsigned int n_lm;
    /// SParsity pattern of the system matrix.
    BlockDynamicSparsityPattern dsp_jac;

    /// Time discretization {
    /// BDF order.
    unsigned int prm_bdf_order;
    /// Time.
    double time;
    /// Time step.
    double delta_t;
    /// Time step number.
    unsigned int timestep_number = 0;
    /// Final time.
    double time_final;
    /// }

    /// Algebraic elements {
    /// Solution vector.
    LinAlg::MPI::BlockVector sol;
    /// Solution vector, without ghost entries.
    LinAlg::MPI::BlockVector sol_owned;
    /// Non-null initial condition for velocity.
    Functions::ParsedFunction<dim> initial_condition_velocity;
    /// Non-null initial condition for displacement.
    Functions::ParsedFunction<dim> initial_condition_mapping;
    /// Preconditioner handler.
    utils::BlockPreconditionerHandler preconditioner;
    /// Linear solver handler.
    utils::LinearSolverHandler<LinAlg::MPI::BlockVector> linear_solver;
    /// global matrix.
    LinAlg::MPI::BlockSparseMatrix matrix;
    /// global rhs.
    LinAlg::MPI::BlockVector rhs;
    /// displacement at previous step.
    LinAlg::MPI::Vector X_n;
    /// }

    /// Data problem {
    /// Fluid density.
    double prm_rho_f;
    /// Structure density.
    double prm_rho_s;
    /// Thickness of the structure.
    double thickness;
    /// Difference densities.
    double delta_rho;
    /// Elastic constant of the structure.
    double prm_kappa;
    /// fluid viscosity
    double prm_viscosity;
    /// }

    /// Mapping {
    /// Mapping of the reference configuration of the solid.
    std::unique_ptr<Mapping<dim,dim>> immersed_mapping_B;
    /// Mapping of the actual configuration of the solid.
    std::unique_ptr<Mapping<dim,dim>> immersed_mapping_Bt;
    /// Mapping of the fluid.
    std::unique_ptr<Mapping<dim,dim>> space_mapping;
    /// }

    /// Create mesh.
    void
    create_mesh();
    /// Setup system, @a i.e. allocate matrices and vectors.
    void
    setup_system();
    /// Make sparsity pattern and initialize the system matrix accordingly.
    void
    make_sparsity();
    ///
    void
    assemble_sparsity();
    /// Solve system.
    void
    solve_system();
    /// Output results.
    void
    output_results() const;
    /// Update time
    void
    update_time();
    /// Assemble matrices.
    void
    assemble_matrices();
    /// Assemble @f$A^{n+1}_{ij}=\frac{\rho_f}{\Delta t}(\varphi_j,\varphi_i)+((u_h^n \cdot \nabla \varphi_j,\varphi_i)-(u_h^n \cdot \nabla \varphi_i,\varphi_j))/2@f$.
    void
    assemble_matrix_A();
    /// Assemble @f$B_{ki} = -(\text{div}\varphi_i, \psi_k)@f$.
    void
    assemble_matrix_B();
    /// Assemble @f$As_{ij} = \frac{\delta_\rho}{\Delta t^2} (\chi_j,\chi_i)_{\mathcal{B}}+\kappa (\nabla_s \chi_j, \nabla_s \chi_i)_{\mathcal{B}}@f$.
    void
    assemble_matrix_As();
    /// Assemble @f$Ls_{lj} = c_2(\zeta_l,\chi_j) = (\zeta_l, \chi_j)_{\mathcal{B}}@f$.
    void
    assemble_matrix_Ls();
    /// Assemble @f$Lf_{lj} = c_1(\zeta_l, \varphi_j(X_h^n)) =  (\zeta_l,  \varphi_j(X_h^n))_{\mathcal{B}}@f$.
    void
    assemble_matrix_Lf();
    /// Assemble @f$f_i = \frac{\rho_f}{\Delta t} ((\varphi_j,\varphi_i) u_h^n)_i@f$.
    void
    assemble_vector_f();
    /// Assemble @f$g_i = \frac{\delta_\rho}{\Delta t^2} (M_s (2 \cdot X_h^n - X_h^{n-1}))_i @f$.
    void
    assemble_vector_g();
    /// Assemble @f$d_l = -\frac{1}{\Delta t} (Ls X_h)_l @f$.
    void
    assemble_vector_d();
  };

  void
  IBM::run()
  {
    setup_system();

    while (time < time_final)
    {
      time += delta_t;
      ++timestep_number;

      assemble_matrices();

      // Update the displacement X_{n-1}
      X_n = sol_owned.block(2);

      solve_system();

      update_time();
    }

    output_results();
  }

  void
  IBM::update_time()
  {
    /// Update mapping and dsp
    immersed_mapping_Bt = std::make_unique<MappingQEulerian<dim, LinAlg::MPI::Vector, dim>> (prm_mapping_degree, dof_handler_m, sol_owned.block(2));
    matrix.clear();
    assemble_sparsity();
    matrix.reinit(owned_dofs, dsp_jac, mpi_comm);
  }

  void
  IBM::declare_parameters(ParamHandler &params) const
  {
    // Default parameters.
    linear_solver.declare_parameters(params);
    preconditioner.declare_parameters(params);

    // Extra parameters.
    params.enter_subsection("Problem");
    {
      params.declare_entry(
        "density fluid",
        "1",
        dealii::Patterns::Double(0),
        "Density of fluid.");
      params.declare_entry(
        "density structure",
        "0.8",
        dealii::Patterns::Double(0),
        "Density of structure.");
      params.declare_entry(
        "thickness",
        "1",
        dealii::Patterns::Double(0),
        "Thickness of structure.");
      params.declare_entry(
        "elastic constant",
        "20",
        dealii::Patterns::Double(0),
        "Elastic constant of the structure.");
      params.declare_entry(
        "viscosity",
        "0.01",
        dealii::Patterns::Double(0),
        "Viscosity of the fluid.");
        params.enter_subsection("Initial condition displacement");
        {
          Functions::ParsedFunction<dim>::declare_parameters(params, dim);
        }
        params.leave_subsection();
        params.enter_subsection("Initial condition velocity");
        {
          Functions::ParsedFunction<dim>::declare_parameters(params, dim);
        }
        params.leave_subsection();
    }
    params.leave_subsection();

    params.enter_subsection("Mesh and space discretization");
    {
      params.declare_entry(
        "filename fluid",
        "../../IBM/mesh/square.msh",
        dealii::Patterns::Anything(),
        "Filename for the fluid domain.");
      params.declare_entry(
        "filename structure",
        "../../IBM/mesh/floating_ball_ref2.msh",
        dealii::Patterns::Anything(),
        "Filename for the structure domain.");
      params.declare_entry(
        "scaling factor",
        "1",
        dealii::Patterns::Double(0),
        "Scaling factor.");
      params.declare_entry(
        "velocity degree",
        "2",
        dealii::Patterns::Integer(1),
        "Polynomial degree for velocity variable.");
      params.declare_entry(
        "pressure degree",
        "1",
        dealii::Patterns::Integer(1),
        "Polynomial degree for pressure variable.");

    }
    params.leave_subsection();

    params.enter_subsection("Time solver");
    {
      params.declare_entry(
        "BDF order",
        "1",
        dealii::Patterns::Integer(1),
        "BDF order for the time discretization.");
      params.declare_entry(
        "initial time",
        "0",
        dealii::Patterns::Double(0),
        "Initial time.");
      params.declare_entry(
        "final time",
        "0.001",
        dealii::Patterns::Double(0),
        "Final time.");
      params.declare_entry(
        "delta t",
        "0.0001",
        dealii::Patterns::Double(0),
        "Time discretization size.");
    }
    params.leave_subsection();
  }

  void
  IBM::parse_parameters(ParamHandler &params)
  {
    // Parse input file.
    params.parse();

    // Read input parameters.
    linear_solver.parse_parameters(params);
    preconditioner.parse_parameters(params);

    // Extra parameters.
    params.enter_subsection("Problem");
    prm_rho_f = params.get_double("density fluid");
    prm_rho_s = params.get_double("density structure");
    delta_rho = prm_rho_s - prm_rho_f; // case codimension = 0
    prm_kappa = params.get_double("elastic constant");
    prm_viscosity = params.get_double("viscosity");
    params.enter_subsection("Initial condition displacement");
    {
      initial_condition_mapping.parse_parameters(params);
    }
    params.leave_subsection();
    params.enter_subsection("Initial condition velocity");
    {
      initial_condition_velocity.parse_parameters(params);
    }
    params.leave_subsection();
    params.leave_subsection();

    params.enter_subsection("Mesh and space discretization");
    prm_file_f = params.get("filename fluid");
    AssertThrow(std::filesystem::exists(prm_file_f),
                dealii::StandardExceptions::ExcMessage(
                    "This mesh file/directory does not exist."));

    prm_file_s = params.get("filename structure");
    AssertThrow(std::filesystem::exists(prm_file_s),
                dealii::StandardExceptions::ExcMessage(
                    "This mesh file/directory does not exist."));

    prm_scaling_factor = params.get_double("scaling factor");
    prm_velocity_degree = params.get_integer("velocity degree");
    prm_pressure_degree = params.get_integer("pressure degree");
    params.leave_subsection();

    params.enter_subsection("Time solver");
    prm_bdf_order = params.get_integer("BDF order");
    time = params.get_double("initial time");
    time_final = params.get_double("final time");
    delta_t = params.get_double("delta t");
    params.leave_subsection();
  }

  void
  IBM::solve_system()
  {
    preconditioner.initialize(matrix);
    linear_solver.solve(matrix, sol_owned, rhs, preconditioner);
    sol = sol_owned;
  }

  void
  IBM::output_results() const
  {
    DataOut<dim> data_out;

    LinAlg::MPI::Vector displacement;
    displacement.reinit(owned_dofs_m, mpi_comm);
    displacement = sol.block(2);

    data_out.add_data_vector(dof_handler_m, displacement, "u");
    data_out.build_patches();
    utils::dataout_write_hdf5(data_out, "solution");

    data_out.clear();
  }

  void
  IBM::create_mesh()
  {
    pcout << "create the mesh"<<std::endl;

    // deal.II does not provide runtime generation of tetrahedral meshes, hence
    // they can currently be imported only from file.
    triangulation_f->initialize_from_file(prm_file_f, prm_scaling_factor);
  //  triangulation_f->set_element_type(utils::MeshHandler::ElementType::Tet);
    triangulation_f->create_mesh();

    triangulation_s->initialize_from_file(prm_file_s, prm_scaling_factor);
  //  triangulation_s->set_element_type(utils::MeshHandler::ElementType::Tet);
    triangulation_s->create_mesh();
  }

  void
  IBM::setup_system()
  {
    // create mesh
    create_mesh();

    // initialize dof handlers and finite elements.
    const auto fe_scalar_velocity =
      triangulation_f->get_fe_lagrange(prm_velocity_degree);
    const auto fe_pressure =
      triangulation_f->get_fe_lagrange(prm_pressure_degree);
    const auto fe_scalar_mapping =
      triangulation_s->get_fe_lagrange(prm_mapping_degree);
    const auto fe_scalar_lm =
      triangulation_s->get_fe_lagrange(prm_lm_degree);

    fe_v = std::make_unique<FESystem<dim>>(*fe_scalar_velocity,
                                       dim);

    fe_p = std::make_unique<FESystem<dim>>(*fe_pressure,
                                        1);

    fe_m = std::make_unique<FESystem<dim>>(*fe_scalar_mapping,
                                          dim);

    fe_lm = std::make_unique<FESystem<dim>>(*fe_scalar_lm,
                                          dim);

    quadrature_formula_f =
      triangulation_f->get_quadrature_gauss(prm_velocity_degree + 1);

      quadrature_formula_s =
        triangulation_f->get_quadrature_gauss(prm_mapping_degree + 1);

    space_mapping = triangulation_f->get_mapping(prm_velocity_degree); // need because MappingQ by default
    immersed_mapping_B = triangulation_s->get_mapping(prm_mapping_degree);

    fe_values_v =
    std::make_unique<FEValues<dim>>(*fe_v,
                                    *quadrature_formula_f,
                                    update_values | update_gradients |
                                      update_quadrature_points |
                                      update_JxW_values);

    fe_values_p =
    std::make_unique<FEValues<dim>>(*fe_p,
                                    *quadrature_formula_f,
                                    update_values | update_gradients |
                                      update_quadrature_points |
                                      update_JxW_values);

    fe_values_m=
    std::make_unique<FEValues<dim>>(*fe_m,
                                      *quadrature_formula_s,
                                      update_values | update_gradients |
                                      update_quadrature_points |
                                      update_JxW_values);

    fe_values_lm=
    std::make_unique<FEValues<dim>>(*fe_lm,
                                    *quadrature_formula_s,
                                    update_values | update_gradients |
                                    update_quadrature_points |
                                    update_JxW_values);

    dof_handler_v.reinit(triangulation_f->get());
    dof_handler_v.distribute_dofs(*fe_v);

    dof_handler_p.reinit(triangulation_f->get());
    dof_handler_p.distribute_dofs(*fe_p);

    dof_handler_m.reinit(triangulation_s->get());
    dof_handler_m.distribute_dofs(*fe_m);

    dof_handler_lm.reinit(triangulation_s->get());
    dof_handler_lm.distribute_dofs(*fe_lm);

    n_u = dof_handler_v.n_dofs(), n_p = dof_handler_p.n_dofs();
    n_m = dof_handler_m.n_dofs(), n_lm = dof_handler_lm.n_dofs();

    pcout<<"for velocity"<<std::endl;
    triangulation_f->get_info().print(prm_subsection_path,
                                  dof_handler_v.n_dofs(),
                                  true);
    pcout<<"for pressure"<<std::endl;
    triangulation_f->get_info().print(prm_subsection_path,
                                  dof_handler_p.n_dofs(),
                                  true);
    pcout<<"for displacement"<<std::endl;
    triangulation_s->get_info().print(prm_subsection_path,
                                  dof_handler_m.n_dofs(),
                                  true);
    pcout<<"for Lagrange multpliers"<<std::endl;
    triangulation_s->get_info().print(prm_subsection_path,
                                  dof_handler_lm.n_dofs(),
                                  true);

     std::string polynomial_type_char = triangulation_f->is_hex() ? "Q" : "P";
     pcout << polynomial_type_char << prm_velocity_degree << "-"
           << polynomial_type_char << prm_pressure_degree << " finite elements"
           << std::endl;

     pcout << "BDF" << prm_bdf_order << " time discretization" << std::endl;

     pcout << utils::log::separator_section << std::endl;

     owned_dofs_v = dof_handler_v.locally_owned_dofs();
     owned_dofs_p = dof_handler_p.locally_owned_dofs();
     owned_dofs_m = dof_handler_m.locally_owned_dofs();
     owned_dofs_lm = dof_handler_lm.locally_owned_dofs();

     owned_dofs.resize(4);
     owned_dofs[0] = owned_dofs_v;
     owned_dofs[1] = owned_dofs_p;
     owned_dofs[2] = owned_dofs_m;
     owned_dofs[3] = owned_dofs_lm;

     DoFTools::extract_locally_relevant_dofs(dof_handler_v, relevant_dofs_v);
     DoFTools::extract_locally_relevant_dofs(dof_handler_p, relevant_dofs_p);
     DoFTools::extract_locally_relevant_dofs(dof_handler_m, relevant_dofs_m);
     DoFTools::extract_locally_relevant_dofs(dof_handler_lm, relevant_dofs_lm);

     relevant_dofs.resize(4);
     relevant_dofs[0] = relevant_dofs_v;
     relevant_dofs[1] = relevant_dofs_p;
     relevant_dofs[2] = relevant_dofs_m;
     relevant_dofs[3] = relevant_dofs_lm;

     // Initialize solutions
     sol.reinit(owned_dofs, relevant_dofs, mpi_comm);
     sol_owned.reinit(owned_dofs, mpi_comm);

     Assert(initial_condition_velocty != nullptr,
            ExcMessage("Initial conditions not set."));

     VectorTools::interpolate(*space_mapping, dof_handler_v,
                              initial_condition_velocity,
                              sol_owned.block(0));

        VectorTools::interpolate(*immersed_mapping_B, dof_handler_m,
                                  initial_condition_mapping,
                                  sol_owned.block(2));
     sol = sol_owned;

     immersed_mapping_Bt = std::make_unique<MappingQEulerian<dim, LinAlg::MPI::Vector, dim>> (prm_mapping_degree, dof_handler_m, sol_owned.block(2));

     // Initialize previous solution for mapping such that if we are solving for N+1, it represents N-1
     // By default when N=1, X_n = 0
     X_n.reinit(owned_dofs_m, relevant_dofs_m, mpi_comm);

     make_sparsity();
  }

  void
  IBM::make_sparsity()
  {
    // Initialize global matrix and rhs
    std::vector<types::global_dof_index> dofs_per_block(4);
    dofs_per_block[0] = n_u;
    dofs_per_block[1] = n_p;
    dofs_per_block[2] = n_m;
    dofs_per_block[3] = n_lm;

    dsp_jac.reinit(dofs_per_block, dofs_per_block);

    // for A
    DoFTools::make_sparsity_pattern (dof_handler_v,
                                     dsp_jac.block(0,0));

    // for B
    DoFTools::make_sparsity_pattern (dof_handler_p, dof_handler_v,
                                     dsp_jac.block(1,0));

    DoFTools::make_sparsity_pattern (dof_handler_v, dof_handler_p,
                                    dsp_jac.block(0,1));
    // for As
    DoFTools::make_sparsity_pattern (dof_handler_m,
                                     dsp_jac.block(2,2));

    // for Ls
    DoFTools::make_sparsity_pattern (dof_handler_lm, dof_handler_m,
                                     dsp_jac.block(3,2));

    DoFTools::make_sparsity_pattern (dof_handler_m, dof_handler_lm,
                                     dsp_jac.block(2,3));

    assemble_sparsity();

    matrix.reinit(owned_dofs, dsp_jac, mpi_comm);

    // global rhs
     rhs.reinit(owned_dofs, mpi_comm);
  }

  void
  IBM::assemble_sparsity()
  {
    NonMatching::create_coupling_sparsity_pattern(dof_handler_v, dof_handler_lm, *quadrature_formula_s, dsp_jac.block(0,3),
    AffineConstraints<double>(), ComponentMask(), ComponentMask(), *space_mapping, *immersed_mapping_Bt);
}

  void
  IBM::assemble_matrices()
  {
    matrix = 0;
    rhs = 0;

    assemble_matrix_A();
    assemble_matrix_B();
    assemble_matrix_As();
    assemble_matrix_Ls();
    assemble_matrix_Lf();

    assemble_vector_f();
    assemble_vector_g();
    assemble_vector_d();
  }

  void
  IBM::assemble_matrix_A()
  {
    const unsigned int dofs_per_cell = fe_v->dofs_per_cell;
    const unsigned int n_q_points    = quadrature_formula_f->size();

    FullMatrix<double> cell_matrix(dofs_per_cell, dofs_per_cell);

    std::vector<types::global_dof_index> dof_indices(dofs_per_cell);

    std::vector<Tensor<1, dim, double>> sol_velocity_loc(
      n_q_points, Tensor<1, dim, double>());

    std::vector<SymmetricTensor<2, dim>> symgrad_phi_u(dofs_per_cell);
    std::vector<Tensor<2, dim>> grad_phi_u(dofs_per_cell);
    std::vector<Tensor<1, dim>> phi_u(dofs_per_cell);

    for (const auto &cell : dof_handler_v.active_cell_iterators())
      {
        if (cell->is_locally_owned())
          {
            cell->get_dof_indices(dof_indices);
            fe_values_v->reinit(cell);

            cell_matrix = 0;

            (*fe_values_v)[velocities].get_function_values(sol, sol_velocity_loc);

            for (unsigned int q = 0; q < n_q_points; ++q)
              {
                for(unsigned int k = 0; k < dofs_per_cell; ++k)
                {
                  symgrad_phi_u[k] = (*fe_values_v)[velocities].symmetric_gradient(k, q);
                  grad_phi_u[k] = (*fe_values_v)[velocities].gradient(k, q);
                  phi_u[k]      = (*fe_values_v)[velocities].value(k, q);
                }

                for (unsigned int i = 0; i < dofs_per_cell; ++i)
                  {
                    for (unsigned int j = 0; j < dofs_per_cell; ++j)
                      {
                        cell_matrix(i,j) += (prm_rho_f/delta_t) * phi_u[i] *
                                            phi_u[j] *
                                             fe_values_v->JxW(q);

                        cell_matrix(i,j) += prm_viscosity * scalar_product(symgrad_phi_u[i], symgrad_phi_u[j]) * fe_values_v->JxW(q);

                        cell_matrix(i,j) += 0.5 * sol_velocity_loc[q] * grad_phi_u[j]
                        * phi_u[i] * fe_values_v->JxW(q);

                        cell_matrix(i,j) +=  -0.5 * sol_velocity_loc[q] * grad_phi_u[i]
                        * phi_u[j] * fe_values_v->JxW(q);
                      }
                  }
              }
          }

          matrix.block(0,0).add(dof_indices, cell_matrix);
      }
  }

  void
  IBM::assemble_matrix_B()
  {
    const unsigned int dofs_per_cell_v = fe_v->dofs_per_cell;
    const unsigned int dofs_per_cell_p = fe_p->dofs_per_cell;
    const unsigned int n_q_points    = quadrature_formula_f->size();

    FullMatrix<double> cell_matrix(dofs_per_cell_p, dofs_per_cell_v);
    FullMatrix<double> cell_matrix_t(dofs_per_cell_v, dofs_per_cell_p);

    std::vector<types::global_dof_index> dof_indices_v(dofs_per_cell_v);
    std::vector<types::global_dof_index> dof_indices_p(dofs_per_cell_p);

    auto cell_p=dof_handler_p.begin();

    std::vector<double> div_phi_u(dofs_per_cell_v);
    std::vector<double>         phi_p(dofs_per_cell_p);

    for (const auto &cell : dof_handler_v.active_cell_iterators()) // same mesh
      {
        if (cell->is_locally_owned())
          {
            cell->get_dof_indices(dof_indices_v);
            cell_p->get_dof_indices(dof_indices_p);

            cell_matrix = 0;
            cell_matrix_t = 0;

            fe_values_v->reinit(cell);
            fe_values_p->reinit(cell_p);

            for (unsigned int q = 0; q < n_q_points; ++q)
              {
                for(unsigned int k=0; k < dofs_per_cell_v; ++k)
                {
                  div_phi_u[k] = (*fe_values_v)[velocities].divergence(k, q);
                }
                for(unsigned int k=0; k < dofs_per_cell_p; ++k)
                {
                    phi_p[k]      = (*fe_values_p)[pressure].value(k, q);
                }
                for (unsigned int i = 0; i < dofs_per_cell_p; ++i)
                  {
                    for (unsigned int j = 0; j < dofs_per_cell_v; ++j)
                      {
                        cell_matrix(i,j) += -div_phi_u[j] *
                                              phi_p [i] *
                                             fe_values_v->JxW(q); // same quadrature formula
                      }
                  }
              }
          }
          ++cell_p;

          cell_matrix_t.copy_transposed(cell_matrix);

          matrix.block(1,0).add(dof_indices_p, dof_indices_v, cell_matrix);
          matrix.block(0,1).add(dof_indices_v, dof_indices_p, cell_matrix_t);
      }
  }

  void
  IBM::assemble_matrix_As()
  {
    const unsigned int dofs_per_cell = fe_m->dofs_per_cell;
    const unsigned int n_q_points    = quadrature_formula_s->size();

    FullMatrix<double> cell_matrix(dofs_per_cell, dofs_per_cell);

    std::vector<types::global_dof_index> dof_indices(dofs_per_cell);

    std::vector<Tensor<2, dim>> grad_phi_m(dofs_per_cell);
    std::vector<Tensor<1, dim>>        phi_m(dofs_per_cell);

    for (const auto &cell : dof_handler_m.active_cell_iterators())
      {
        if (cell->is_locally_owned())
          {
            cell->get_dof_indices(dof_indices);
            fe_values_m->reinit(cell);

            cell_matrix = 0;

            for (unsigned int q = 0; q < n_q_points; ++q)
              {
                for(unsigned int k = 0; k < dofs_per_cell; ++k)
                {
                  grad_phi_m[k] = (*fe_values_m)[disp].gradient(k, q);
                  phi_m[k]      = (*fe_values_m)[disp].value(k, q);
                }
                for (unsigned int i = 0; i < dofs_per_cell; ++i)
                  {
                    for (unsigned int j = 0; j < dofs_per_cell; ++j)
                      {
                       cell_matrix(i,j) += (delta_rho /std::pow(delta_t,2)) * phi_m[i] *
                                             phi_m[j] *
                                             fe_values_m->JxW(q);

                        cell_matrix(i,j) += prm_kappa * scalar_product(grad_phi_m[i],grad_phi_m[j]) * fe_values_m->JxW(q);
                      }
                  }
              }
          }

          matrix.block(2,2).add(dof_indices, cell_matrix);
      }
  }

  void
  IBM::assemble_matrix_Ls()
  {
    const unsigned int dofs_per_cell_m = fe_m->dofs_per_cell;
    const unsigned int dofs_per_cell_lm = fe_lm->dofs_per_cell;
    const unsigned int n_q_points    = quadrature_formula_s->size();

    std::vector<types::global_dof_index> dof_indices_lm(dofs_per_cell_lm);
    std::vector<types::global_dof_index> dof_indices_m(dofs_per_cell_m);

    FullMatrix<double> cell_matrix(dofs_per_cell_lm, dofs_per_cell_m);
    FullMatrix<double> cell_matrix_t(dofs_per_cell_m, dofs_per_cell_lm);

    std::vector<Tensor<1, dim>>        phi_m(dofs_per_cell_m);
    std::vector<Tensor<1, dim>>         phi_lm(dofs_per_cell_lm);

    auto cell_lm=dof_handler_lm.begin();

    for (const auto &cell : dof_handler_m.active_cell_iterators()) // same mesh
      {
        if (cell->is_locally_owned())
          {
            cell->get_dof_indices(dof_indices_m);
            cell_lm->get_dof_indices(dof_indices_lm);

            fe_values_m->reinit(cell);
            fe_values_lm->reinit(cell_lm);

            cell_matrix = 0;
            cell_matrix_t = 0;

            for (unsigned int q = 0; q < n_q_points; ++q)
              {
                for(unsigned int k = 0; k < dofs_per_cell_m; ++k)
                {
                    phi_m[k]      = (*fe_values_m)[disp].value(k, q);
                }
                for(unsigned int k = 0; k < dofs_per_cell_lm; ++k)
                {
                    phi_lm[k]      = (*fe_values_lm)[lagrange_mult].value(k, q);
                }
                for (unsigned int i = 0; i < dofs_per_cell_lm; ++i)
                  {
                    for (unsigned int j = 0; j < dofs_per_cell_m; ++j)
                      {
                        cell_matrix(j,i) += -phi_lm[i]  *
                                             phi_m[j] *
                                             fe_values_m->JxW(q); // same quadrature formula
                      }
                  }
              }
          }
          ++cell_lm;

          cell_matrix_t.copy_transposed(cell_matrix);

          matrix.block(3,2).add(dof_indices_lm, dof_indices_m, cell_matrix);
          matrix.block(2,3).add(dof_indices_m, dof_indices_lm, cell_matrix_t);
      }
  }

  void
  IBM::assemble_matrix_Lf()
  {
    NonMatching::create_coupling_mass_matrix(dof_handler_v,
                                     dof_handler_lm,
                                     *quadrature_formula_s,
                                     matrix.block(0,3),
                                     AffineConstraints<double>(),
                                     ComponentMask(),
                                     ComponentMask(),
                                     *space_mapping, *immersed_mapping_Bt);

    for(unsigned int lm = 0; lm < n_lm; ++lm)
    {
    for(unsigned int v=0; v < n_u; ++v)
    {
      matrix.block(3,0).set(lm,v,matrix.block(0,3)(v,lm));
    }
  }
}


  void
  IBM::assemble_vector_f()
  {
    const unsigned int dofs_per_cell = fe_v->dofs_per_cell;
    const unsigned int n_q_points    = quadrature_formula_f->size();

    Vector<double> cell_rhs(dofs_per_cell);

    std::vector<types::global_dof_index> dof_indices(dofs_per_cell);

    std::vector<Tensor<1, dim, double>> sol_velocity_loc(
      n_q_points, Tensor<1, dim, double>());
    std::vector<Tensor<1, dim>> phi_u(dofs_per_cell);

    for (const auto &cell : dof_handler_v.active_cell_iterators())
      {
        if (cell->is_locally_owned())
          {
            cell->get_dof_indices(dof_indices);
            fe_values_v->reinit(cell);

            (*fe_values_v)[velocities].get_function_values(sol, sol_velocity_loc);

            cell_rhs = 0;

            for (unsigned int q = 0; q < n_q_points; ++q)
              {
                for(unsigned int k = 0; k < dofs_per_cell; ++k)
                {
                  phi_u[k]      = (*fe_values_v)[velocities].value(k, q);
                }

                for (unsigned int i = 0; i < dofs_per_cell; ++i)
                  {
                     cell_rhs(i) +=(prm_rho_f/delta_t) * sol_velocity_loc[q] * phi_u[i] *
                                             fe_values_v->JxW(q);

                  }
              }
          }

          rhs.block(0).add(dof_indices, cell_rhs);
      }
  }

  void
  IBM::assemble_vector_g()
  {
    const unsigned int dofs_per_cell = fe_m->dofs_per_cell;
    const unsigned int n_q_points    = quadrature_formula_s->size();

    std::vector<types::global_dof_index> dof_indices(dofs_per_cell);

    std::vector<Tensor<1, dim, double>> sol_mapping_loc(
      n_q_points, Tensor<1, dim, double>());

      std::vector<Tensor<1, dim, double>> sol_mapping_n_loc(
        n_q_points, Tensor<1, dim, double>());

    std::vector<Tensor<1, dim>> phi_m(dofs_per_cell);

    Vector<double> cell_rhs(dofs_per_cell);

    for (const auto &cell : dof_handler_m.active_cell_iterators())
      {
        if (cell->is_locally_owned())
          {
            cell->get_dof_indices(dof_indices);
            fe_values_m->reinit(cell);

            cell_rhs = 0;

            (*fe_values_m)[disp].get_function_values(sol.block(2), sol_mapping_loc);
            (*fe_values_m)[disp].get_function_values(X_n, sol_mapping_n_loc);

            for (unsigned int q = 0; q < n_q_points; ++q)
              {
                for(unsigned int k = 0; k < dofs_per_cell; ++k)
                {
                  phi_m[k]      = (*fe_values_m)[disp].value(k, q);
                }

                for (unsigned int i = 0; i < dofs_per_cell; ++i)
                  {
                    cell_rhs(i) += (prm_rho_f/delta_t) * (2*sol_mapping_loc[q]-sol_mapping_n_loc[q]) * phi_m[i] *
                                             fe_values_m->JxW(q);
                  }
              }
          }

          rhs.block(2).add(dof_indices, cell_rhs);
      }
  }

  void
  IBM::assemble_vector_d()
  {
    for(unsigned int i = 0; i < matrix.block(3,2).m(); ++i)
    {
      for(unsigned int j = 0; j < matrix.block(3,2).n(); ++j)
      {
        rhs.block(3)[i] += (1/delta_t) * matrix.block(3,2)(i,j) * sol_owned.block(2)[j];
      }
    }
  }



} // namespace lifex

#endif /* LIFEX_FLUID_DYNAMICS_IBM_HPP_ */
